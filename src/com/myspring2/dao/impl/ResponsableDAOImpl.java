/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myspring2.dao.impl;

import com.myspring2.beans.Responsable;
import com.myspring2.dao.ResponsableDAO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

/**
 *
 * @author gianburga
 */
public class ResponsableDAOImpl extends JdbcDaoSupport implements ResponsableDAO {

    @Override
    public Responsable getById(Integer id) {
        String SQL = "select id, dni, nombre, apellido from responsable where id=?";

        List<Responsable> responsables = getJdbcTemplate().query(SQL, new Object[]{id}, new RowMapper(){
            @Override
            public Object mapRow(ResultSet rs, int i) throws SQLException {
                Responsable responsable = new Responsable(
                        rs.getInt("id"), 
                        rs.getString("dni"), 
                        rs.getString("nombre"),
                        rs.getString("apellido"));
                return responsable;
            }
        });
        
        return responsables.size() > 0 ? responsables.get(0) : null;
    }

    @Override
    public void save(Responsable responsable) {
        String SQL = "insert into responsable (dni, nombre, apellido) values (?, ?, ?)";
        getJdbcTemplate().update(SQL, new Object[]{
            responsable.getDNI(),
            responsable.getNombre(),
            responsable.getApellido()
        });
    }

    @Override
    public List<Responsable> getAll() {
        String SQL = "select id, dni, nombre, apellido from responsable";
        List<Responsable> responsables = getJdbcTemplate().query(SQL, new Object[]{}, new RowMapper(){
            @Override
            public Object mapRow(ResultSet rs, int i) throws SQLException {
                Responsable responsable = new Responsable(
                        rs.getInt("id"), 
                        rs.getString("dni"), 
                        rs.getString("nombre"),
                        rs.getString("apellido"));
                return responsable;
            }
        });
        return responsables;
    }
}
