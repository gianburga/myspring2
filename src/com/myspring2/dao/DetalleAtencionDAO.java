/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myspring2.dao;

import com.myspring2.beans.Atencion;
import com.myspring2.beans.DetalleAtencion;
import java.util.List;

/**
 *
 * @author gianburga
 */
public interface DetalleAtencionDAO {
    public DetalleAtencion getById(Integer id);
    public void save(DetalleAtencion detalleAtencion);
    public List<DetalleAtencion> getByAtencion(Atencion atencion);
}
