/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myspring2.dao;

import com.myspring2.beans.Atencion;
import com.myspring2.beans.DetalleAtencion;
import java.util.List;

/**
 *
 * @author gianburga
 */
public interface AtencionDAO {
    public Atencion getById(Integer id);
    public void save(Atencion atencion);
    public void addDetalle(Atencion atencion, DetalleAtencion detalleAtencion);
    public List<Atencion> getAll();
}
