/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myspring2.dao;

import com.myspring2.beans.Empresa;
import java.util.List;

/**
 *
 * @author gianburga
 */
public interface EmpresaDAO {
    public Empresa getById(Integer id);
    public void save(Empresa empresa);
    public List<Empresa> getAll();
}
