/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myspring2;

import com.myspring2.beans.Responsable;
import com.myspring2.dao.ResponsableDAO;
import com.myspring2.dao.impl.ResponsableDAOImpl;
import com.myspring2.services.ResponsableService;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author gianburga
 */
public class MySpring2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        // ResponsableDAO responsableDAO = (ResponsableDAOImpl) context.getBean("responsableDAOImpl");
        
        ResponsableService responsableService = (ResponsableService) context.getBean("responsableService");
        
        for (Responsable responsable: responsableService.getAll()) {
            System.out.println(responsable);
        }
        
        /*
        System.out.println("Responsable getById");
        Integer id = 0;
        try {
            id = Integer.parseInt(javax.swing.JOptionPane.showInputDialog("Ingrese id"));
            System.out.println("Buscando...");
            Responsable responsable = responsableDAO.getById(id);
            if (responsable != null) {
                System.out.println(responsable);
            } else {
                System.out.println("Responsable no existe.");
            }
        } catch (NumberFormatException e){
            System.out.println("id incorrecto!");
        }
        
        String nombre = javax.swing.JOptionPane.showInputDialog("Ingrese nombre:");
        String apellido = javax.swing.JOptionPane.showInputDialog("Ingrese apellido:");
        String dni = javax.swing.JOptionPane.showInputDialog("Ingrese dni:");
        if (nombre.trim().length() > 0 && apellido.trim().length() > 0 && dni.trim().length() > 0) {
            Responsable responsable = new Responsable(null, dni, nombre, apellido);
            responsableDAO.save(responsable);
        } else {
            System.out.println("Datos vacios.");
        }
        */
        
        /*ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        DriverManagerDataSource dataSource = (DriverManagerDataSource) context.getBean("myDataSource");
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select * from responsable");
            
            while(resultSet.next()){
                System.out.println("Persona " + resultSet.getInt("id"));
                System.out.println("Nombre: " + resultSet.getString("nombre"));
                System.out.println("Apellido: " + resultSet.getString("apellido"));
                System.out.println("DNI: " + resultSet.getString("dni"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState());
        } finally {
            try {
                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();
                if (connection != null) connection.close();
            } catch (SQLException ex) {
                System.out.println(ex.getSQLState());
            }
        }*/
    }
    
}
